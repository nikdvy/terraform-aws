provider "aws" {
    region     = "eu-central-1"
}

resource "aws_instance" "test-tf-rhel" {
    ami = "ami-06ec8443c2a35b0ba"
    instance_type = "t2.micro"
    tags = {
        Name = "rhel"
        Owner = "nikdvy"
        Project = "terraform learning"
    }
}

resource "aws_instance" "test-tf-al" {
    ami = "ami-0bd99ef9eccfee250"
    instance_type = "t2.micro"
    tags = {
        Name = "amazon linux"
        Owner = "nikdvy"
        Project = "terraform learning"
    }
}
