provider "aws" {
    region = "eu-central-1"
}

locals {
    ingress_port = [
        # 2 tuples of port's description
        {
            port = "80",
            description = "http"
        },
        {
            port = "443",
            description = "https"
        }
    ]
}

resource "aws_security_group" "web-server" {
  name = "WebServerSecurityGroup"
  description = "Dynamic Security Group"

    dynamic "ingress" {
        for_each = local.ingress_port
        content {
            from_port = ingress.value.port
            to_port = ingress.value.port
            protocol = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
            }
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["10.10.0.0/16"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "web-server"
    }
}