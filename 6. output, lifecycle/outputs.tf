output "web_server_instance_id" {
    value = aws_instance.web-server.id
    description = "Instance ID"
}

output "web_server_public_ip" {
    value = aws_eip.web-server.public_ip
    description = "Elastic IP"
}

output "web_server_security_group_id" {
    value = aws_security_group.web-server.id
    description = "Security group ID"
}

output "web_server_ssh_key" {
    value = aws_instance.web-server.key_name
    description = "Instance ssh-key's name for connection"
}