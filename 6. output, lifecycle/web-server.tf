provider "aws" {
    region = "eu-central-1"
}

locals {
    ingress_port = [
        # 2 tuples of port's description
        {
            port = "80",
            description = "http"
        },
        {
            port = "443",
            description = "https"
        }
    ]
}

resource "aws_eip" "web-server" {
    instance = aws_instance.web-server.id
    tags = {
        Name = "web-server"
    }
}

resource "aws_instance" "web-server" {
    ami = "ami-0bd99ef9eccfee250"
    instance_type = "t2.micro"
    key_name = "aws2"
    vpc_security_group_ids = [aws_security_group.web-server.id]
    user_data = templatefile("user_data.tpl",
    { 
        first_name = "nikdvy",
        last_name = "d",
        names = ["vasya","kolya","petya","john","donald","masha"]
    })
    tags = {
        Name = "web-server"
        Owner = "nikolai_dvoinishnikov@epam.com"
    }
    lifecycle {
        #ignore_changes = [ami, user_data] # the changes in ami and user_data doesn't apply anymore
        create_before_destroy = true
    }
}

resource "aws_security_group" "web-server" {
    name = "WebServerSecurityGroup"
    description = "Dynamic Security Group"

    dynamic "ingress" {
        for_each = local.ingress_port
        content {
            from_port = ingress.value.port
            to_port = ingress.value.port
            protocol = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["62.16.44.103/32"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "web-server"
    }
}