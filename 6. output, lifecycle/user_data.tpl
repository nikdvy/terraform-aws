#!/bin/bash
yum -y update
yum -y install httpd


cat <<EOF > /var/www/html/index.html
<html>
<h2>Build by power of Terraform <font color = "red"> v1.0.12</font></h2><br> 
Owner is ${first_name} ${last_name}<br>
%{ for x in names ~}
Hello to ${x} by ${first_name}!<br>
%{ endfor ~}
</html>
EOF

sudo service httpd start
chkconfig httpd on