provider "aws" {
    region = "eu-central-1"
}

resource "aws_instance" "web-server" {
    ami = "ami-0bd99ef9eccfee250"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.web-server.id]
    user_data = templatefile("user_data.tpl",
    { 
        first_name = "nikdvy",
        last_name = "d",
        names = ["vasya","kolya","petya","john","donald","masha"]
    })
    tags = {
        Name = "web-server"
    }
}

lifecycle {
    
}

resource "aws_security_group" "web-server" {
  name = "WebServerSecurityGroup"
  description = "MyFirstSG"

  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
      Name = "web-server"
  }
}
