### Terraform version: v1.0.11
### AWS plugin version: v3.66.0

#### For to try theese projects you have to get in specified 'training*' directory and run commands below:

```bash
export AWS_ACCESS_KEY_ID=***
export AWS_SECRET_ACCESS_KEY=***
terraform init
terraform plan
terraform apply -auto-approve
```
