terraform { 
    required_providers {
        aws = {
            version = "~> 3.69"
        }
    }
    backend "s3" {
        bucket = "wordpress-nikdvy"
        key = "AWS/Dev/terraform-states/terraform.tfstate"
        region = "eu-central-1"
    }
}

provider "aws" {
    region = "eu-central-1"
}