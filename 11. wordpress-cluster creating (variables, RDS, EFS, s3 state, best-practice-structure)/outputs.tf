output "selected_availability_zone_1" {
    value = data.aws_availability_zones.available.names[0]
}

output "selected_availability_zone_2" {
    value = data.aws_availability_zones.available.names[1]
}

output "load_balancer_dns_name" {
    value = aws_elb.wp_elb.dns_name
}