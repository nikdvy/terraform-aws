// create db subnet group
resource "aws_db_subnet_group" "wp_subnet_rds" {
  name       = "main"
  subnet_ids = [
      aws_subnet.wp_subnet_1.id,
      aws_subnet.wp_subnet_2.id
    ]

  tags = {
    Name = "wp_db_subnet_group"
  }
}

// create db subnet
resource "aws_db_instance" "wp_rds" {
    allocated_storage    = 10
    engine               = "mysql"
    engine_version       = "5.7"
    instance_class       = "db.t2.micro"
    name                 = "wpdb"
    username             = "wpuser"
    password             = "qwertyuiop"
    parameter_group_name = "default.mysql5.7"
    skip_final_snapshot  = true
    allow_major_version_upgrade = false
    db_subnet_group_name = aws_db_subnet_group.wp_subnet_rds.name
    vpc_security_group_ids = [
        aws_security_group.wp_sg_rds.id
    ]

    tags = {
        Name = "wp_rds",
        Owner = "nikdvy"
    }
}