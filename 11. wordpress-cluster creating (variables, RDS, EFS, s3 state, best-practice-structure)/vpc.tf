// create virtual private cloude
resource "aws_vpc" "wp_vpc" {
    cidr_block = "10.10.0.0/16"


    tags = {
        Name = "wp_vpc"
        Owner = "nikdvy"
    }
}

// create subnets
resource "aws_subnet" "wp_subnet_1" {
    vpc_id = aws_vpc.wp_vpc.id
    cidr_block = "10.10.1.0/24"
    availability_zone = data.aws_availability_zones.available.names[0]

    tags = {
        Name = "wp_subnet_1"
        Owner = "nikdvy"
    }
}

resource "aws_subnet" "wp_subnet_2" {
    vpc_id = aws_vpc.wp_vpc.id
    cidr_block = "10.10.2.0/24"
    availability_zone = data.aws_availability_zones.available.names[1]

    tags = {
        Name = "wp_subnet_2"
        Owner = "nikdvy"
    }
}

// create internet gateway allowing get internet connection
resource "aws_internet_gateway" "wp_igw" {
    vpc_id = aws_vpc.wp_vpc.id

    tags = {
        Name = "wp_igw"
        Owner = "nikdvy"
    }
}

// create specified route table
resource "aws_route_table" "wp_rt" {
    vpc_id = aws_vpc.wp_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.wp_igw.id
    }

    tags = {
        Name = "wp_rt"
    }
}

// associate the route table
resource "aws_route_table_association" "wp_rt_ass_1" {
    subnet_id = aws_subnet.wp_subnet_1.id
    route_table_id = aws_route_table.wp_rt.id
}

resource "aws_route_table_association" "wp_rt_ass_2" {
    subnet_id = aws_subnet.wp_subnet_2.id
    route_table_id = aws_route_table.wp_rt.id
}

// create security groups
resource "aws_security_group" "wp_sg_elb" {
    name = "wp_sg_elb"
    description = "Security group for wp-elb"
    vpc_id = aws_vpc.wp_vpc.id

    dynamic "ingress" {
        for_each = [
            "80",
            "443",
            "8080"
        ]
        content {
            from_port = ingress.value
            to_port = ingress.value
            protocol = "tcp"
            cidr_blocks = [
                "0.0.0.0/0"
            ]
        }
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "0.0.0.0/0"
        ]
        ipv6_cidr_blocks =[
            "::/0"
        ]
    }

    tags = {
        Name = "wp_sg_elb"
    }
}

resource "aws_security_group" "wp_sg_instance" {
    name = "wp_sg_instance"
    description = "Security group for wp-instances"
    vpc_id = aws_vpc.wp_vpc.id
    dynamic "ingress" {
        for_each = ["80", "22"]
        content {
            from_port = ingress.value
            to_port = ingress.value
            protocol =  "tcp"
            cidr_blocks = [
                "0.0.0.0/0"
            ]
        }
    }

    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = [
            "10.10.0.0/16"
        ]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "0.0.0.0/0"
        ]
    }

    tags = {
        Name = "wp_sg_instance"
    }
}

resource "aws_security_group" "wp_sg_rds" {
    name = "wp_sg_rds"
    description = "Security group for wp-rds"
    vpc_id = aws_vpc.wp_vpc.id

    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        security_groups = [
            aws_security_group.wp_sg_instance.id
        ]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "0.0.0.0/0"
        ]
    }

    tags = {
        Name = "wp_sg_rds"
    }
}

resource "aws_security_group" "wp_sg_efs" {
    name = "wp_sg_efs"
    description = "Security group for wp-efs"
    vpc_id = aws_vpc.wp_vpc.id
    
    dynamic "ingress" {
        for_each = ["111", "2049"]
        content {
            from_port = ingress.value
            to_port = ingress.value
            protocol =  "tcp"
            cidr_blocks = [
                "10.10.0.0/16"
            ]
        }
    }
    
    dynamic "ingress" {
        for_each = ["111", "2049"]
        content {
            from_port = ingress.value
            to_port = ingress.value
            protocol =  "udp"
            cidr_blocks = [
                "10.10.0.0/16"
            ]
        }
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "0.0.0.0/0"
        ]
    }

    tags = {
        Name = "wp_sg_efs"
    }
}