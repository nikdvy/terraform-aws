// create efs
resource "aws_efs_file_system" "wp_efs" {
    tags = {
        Name = "wp_efs",
        Owner = "nikdvy"
    }
}

// create mount target (for efs attaching to instances)
resource "aws_efs_mount_target" "wp_mt_1" {
    file_system_id = aws_efs_file_system.wp_efs.id
    subnet_id = aws_subnet.wp_subnet_1.id
    security_groups = [
        aws_security_group.wp_sg_efs.id
    ]
}

resource "aws_efs_mount_target" "wp_mt_2" {
    file_system_id = aws_efs_file_system.wp_efs.id
    subnet_id = aws_subnet.wp_subnet_2.id
    security_groups = [
        aws_security_group.wp_sg_efs.id
    ]
}