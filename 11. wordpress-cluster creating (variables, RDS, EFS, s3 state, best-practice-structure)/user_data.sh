#!/bin/bash
sudo yum install httpd php php-mysql -y
rm -rf /var/www/html/*
wget https://wordpress.org/wordpress-5.1.11.tar.gz
tar -xzf wordpress-5.1.11.tar.gz
cp -r wordpress/* /var/www/html/
rm -rf wordpress wordpress-5.1.11.tar.gz
sudo chmod -R 755 /var/www/html/wp-content
sudo chown -R apache:apache /var/www/html/wp-content
sudo systemctl start httpd
sudo systemctl enable httpd
sudo chkconfig httpd on