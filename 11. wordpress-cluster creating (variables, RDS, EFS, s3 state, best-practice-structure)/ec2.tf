// choose instance image
data "aws_ami" "al2" {
    most_recent = true
    owners = ["137112412989"]
    filter {
        name = "name"
        values = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
    }
}

// create launch configuration for autoscaling group
resource "aws_launch_configuration" "wp_alc" {
    name_prefix = "wp_alc-"
    image_id = data.aws_ami.al2.id
    instance_type = "t2.micro"
    key_name = var.key_pair
    user_data = data.local_file.user_data.content
    security_groups = [ 
        aws_security_group.wp_sg_instance.id
    ]
    associate_public_ip_address = true

    lifecycle {
        create_before_destroy = true
    }
}

// create autoscaling group
resource "aws_autoscaling_group" "wp_asg" {
    name = "wp_asg_with_${aws_launch_configuration.wp_alc.name}"
    min_size = 1
    desired_capacity = 2
    max_size = 3
    min_elb_capacity = 2
    health_check_type = "ELB"
    launch_configuration = aws_launch_configuration.wp_alc.name
    vpc_zone_identifier = [
        aws_subnet.wp_subnet_1.id,
        aws_subnet.wp_subnet_2.id
    ]
    load_balancers = [
        aws_elb.wp_elb.name
    ]

    dynamic "tag" {
        for_each = {
            Name = "wp_instance"
            Owner = "nikdvy"
        }
        content {
            key = tag.key
            value = tag.value
            propagate_at_launch = true
        }
    }

    lifecycle {
        create_before_destroy = true
    }

    
    depends_on = [
        aws_elb.wp_elb
    ]
}

// create classic load balancer
resource "aws_elb" "wp_elb" {
    name = "wpelb"
    subnets = [ 
        aws_subnet.wp_subnet_1.id,
        aws_subnet.wp_subnet_2.id       
    ]
    security_groups = [
        aws_security_group.wp_sg_elb.id
    ]

    listener {
        instance_port = 80
        instance_protocol = "http"
        lb_port = 80
        lb_protocol = "http"
    }

    health_check {
        healthy_threshold = 2
        unhealthy_threshold = 2
        timeout = 3
        target = "HTTP:80/wp-admin/setup-config.php"
        interval = 10
    }

    tags = {
        Name = "wp_elb"
        Owner = "nikdvy"
    }

    depends_on = [
        aws_security_group.wp_sg_elb
    ]
}