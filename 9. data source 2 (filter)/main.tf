provider "aws" {
    region = "eu-central-1"
}

data "aws_ami" "ubuntu_latest" {
    owners = ["099720109477"]
    most_recent = true
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }
}

data "aws_ami" "amazon_linux_latest" {
    owners = ["amazon"]
    most_recent = true
    filter {
        name = "name"
        values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
    }
}

data "aws_ami" "centos_latest" {
    most_recent = true
    owners = ["309956199498"]
    filter {
        name = "name"
        values = ["RHEL_HA-8.4.0_HVM-*-x86_64-*"]
    }
}

# centos
output "centos_ami_id" {
    value = data.aws_ami.centos_latest.id
}

output "centos_ami_name" {
    value = data.aws_ami.centos_latest.name
}

# ubuntu
output "ubuntu_ami_id" {
    value = data.aws_ami.ubuntu_latest.id
}

output "ubuntu_ami_name" {
    value = data.aws_ami.ubuntu_latest.name
}

# amazon linux
output "amazon_linux_ami_id" {
    value = data.aws_ami.amazon_linux_latest.id
}

output "amazon_linux_ami_name" {
    value = data.aws_ami.amazon_linux_latest.name
}