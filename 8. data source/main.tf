provider "aws" {}

data "aws_availability_zones" "working" {}
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
data "aws_vpcs" "my_vpcs" {}
data "aws_vpc" "my-vpc-10" {
    tags = {
        Name = "my-vpc-10"
    }
}

resource "aws_subnet" "test-10" {
    vpc_id = data.aws_vpc.my-vpc-10.id
    availability_zone = data.aws_availability_zones.working.names[0]
    cidr_block = "10.10.10.0/24"
    
    tags = {
        Name = "uat-10 in ${data.aws_availability_zones.working.names[0]}"
        Account = data.aws_caller_identity.current.account_id
        Region = data.aws_region.current.description
    }
}


# outputs of gathered data
# output of 1st element of available availability zones
output "data_aws_availability_zones" {
    value = data.aws_availability_zones.working.names[1]
}
# output aws account ID
output "data_aws_caller_identity" {
    value = data.aws_caller_identity.current.account_id
}
# output current region name
output "data_aws_region_name" {
    value = data.aws_region.current.name
}
# output current region description
output "data_aws_region_description" {
    value = data.aws_region.current.description
}
# aws vpc list
output "data_aws_vpcs" {
    value = data.aws_vpcs.my_vpcs.ids
}
# output my-vpc-10 id and cidr_blocks
output "data_aws_vpc_id" {
    value = data.aws_vpc.my-vpc-10.id
}
output "data_aws_vpc_cidr_block" {
    value = data.aws_vpc.my-vpc-10.cidr_block
}