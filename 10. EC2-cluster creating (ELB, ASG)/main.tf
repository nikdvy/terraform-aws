provider "aws" {
    region = "eu-central-1"
}

data "aws_availability_zones" "available" {}

data "aws_ami" "amazonl_latest" {
    owners = ["amazon"]
    most_recent = true
    filter {
        name = "name"
        values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
    }
}

resource "aws_security_group" "web" {
    
    dynamic "ingress" {
        for_each = [80,443,8080]
        content {
            from_port = ingress.value
            to_port = ingress.value
            protocol = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }
    dynamic "egress" {
        for_each = [0]
        content {
            from_port = egress.value
            to_port = egress.value
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }
}

resource "aws_launch_configuration" "web" {
    //name = "High-available_web-server_LC"
    name_prefix = "High-available_web-server_LC-"
    image_id = data.aws_ami.amazonl_latest.id
    instance_type = "t2.micro"
    security_groups = [aws_security_group.web.id]
    user_data = file("user_data.sh")
    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "web" {
    name = "High-available_web-server_ASG_with_${aws_launch_configuration.web.name}"
    launch_configuration = aws_launch_configuration.web.name
    min_size = 2
    max_size = 2
    min_elb_capacity = 2
    health_check_type = "ELB"
    vpc_zone_identifier = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
    load_balancers = [aws_elb.web.name]
    
    dynamic "tag" {
        for_each = {
            Name = "Web-server in ASG"
            Owner = "nikolai_dvoinishnikov"
            #tag-key = "tag-value"
        }

        content {
            key = tag.key
            value = tag.value
            propagate_at_launch = true
        }
    }

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_elb" "web" {
    name = "webserver-ha-elb"
    availability_zones = [
        data.aws_availability_zones.available.names[0],
        data.aws_availability_zones.available.names[1]
    ]
    security_groups = [aws_security_group.web.id]
    
    listener {
        lb_port = 80
        lb_protocol = "http"
        instance_port = 80
        instance_protocol = "http"
    }

    health_check {
        healthy_threshold = 2
        unhealthy_threshold = 2
        timeout = 3
        target = "HTTP:80/"
        interval = 10
    }

    tags = {
        Name = "Web-server highly-available ELB"
    }
}

resource "aws_default_subnet" "default_az1" {
    availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "default_az2" {
    availability_zone = data.aws_availability_zones.available.names[1]
}
