output "availability_zones_list" {
    value = data.aws_availability_zones.available.names
}
output "latest_amazon_linux_ami" {
    value = data.aws_ami.amazonl_latest.id
}
output "web_loadbalancer_url" {
    value = aws_elb.web.dns_name
}
