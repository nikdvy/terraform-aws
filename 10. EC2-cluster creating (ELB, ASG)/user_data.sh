#!/bin/bash

yum -y update
yum -y install httpd
myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
cat <<EOF > /var/www/html/index.html
<html>
<body bgcolor="black">
<h2><font color="gold"><Build by power of Terraform!<font color="red"> v.1.0.11</font></h2><br><p>
<font color="green">Server private ip-address is <font color="aqua">$myip<br><br>
<font color="magenta">
<b>Version 2.0</b>
</body>
</html>
EOF

sudo service httpd start
chkconfig httpd on
